<?php

//Before you even start you want to know where you
//want to read the XML data from


//load the url where the RSS feed is comming fom
$feed_url = 'http://php.net/feed.atom';


//formats the XML and loads it
$feed = simplexml_load_file($feed_url);


//limit the result we want to pull in
$limit = 5;
$x = 1;


?>


<ul>
	<?php
	//looping through the XML and spitting the title and url
	foreach ($feed -> entry as $item ) {
			
		//we are only limting our results to 5	
		if($x <= $limit){
			$tittle = $item->title;
			$url = $item->id;
			
			echo '<li><a href="', $url, '">', $tittle, '</a></li>';
		}
		
		$x++;
		
	}
	?>
</ul>